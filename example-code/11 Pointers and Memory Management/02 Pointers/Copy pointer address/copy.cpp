#include <iostream>
#include <string>
using namespace std;


int main()
{
	int a = 2, b = 4, c = 6;

	cout << "a address: " << &a << endl;
	cout << "b address: " << &b << endl;
	cout << "c address: " << &c << endl;

	int* ptr1 = nullptr;
	int* ptr2 = nullptr;

	ptr1 = &a;
	ptr2 = ptr1;	// copy the address that ptr1 is pointing to

	cout << "Ptr1 pointing to: " << ptr1 << endl;
	cout << "Ptr1's address: " << &ptr1 << endl;
	cout << "Ptr2 pointing to: " << ptr2 << endl;
	cout << "Ptr2's address: " << &ptr2 << endl;


	while (true);

	return 0;
}
