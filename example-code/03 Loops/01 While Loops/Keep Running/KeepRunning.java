import java.util.Scanner;

public class KeepRunning
{
    static Scanner input = new Scanner( System.in );
    
    public static void main( String args[] )
    {
        boolean done = false;
        while ( !done )
        {
            System.out.print( "Enter a word: " );
            String word;
            word = input.next();

            System.out.print( "The length of that word is "
                + word.length() + " characters." );

            System.out.println( "\n\n\t Do again? (y/n)" );
            String choice;
            choice = input.next();

            // Stop looping if the user is done.
            if ( choice.equals( "n" ) || choice.equals( "N" ) )
            {
                done = true;
            }
        }
    }
}


