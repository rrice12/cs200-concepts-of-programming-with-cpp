import java.util.Scanner;

public class ForLoop
{
    public static void main( String[] args )
    {
        System.out.println( "\n\n Loop A:" );
        for ( int i = 0; i < 10; i++ )
        {
                System.out.print( i + "\t" );
        }

        System.out.println( "\n\n Loop B:" );
        for ( int i = 0; i < 10; i += 2 )
        {
                System.out.print( i + "\t" );
        }

        System.out.println( "\n\n Loop C:" );
        for ( int i = 10; i > 0; i-- )
        {
                System.out.print( i + "\t" );
        }

        System.out.println( "\n\n Loop D:" );
        for ( int i = 1; i < 1000; i *= 5 )
        {
                System.out.print( i + "\t" );
        }
        
    }
}
