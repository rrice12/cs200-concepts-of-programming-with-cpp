#include <iostream>
#include <string>
using namespace std;

int main()
{
	const int MAX_CLASSES = 3;
	string classes[MAX_CLASSES];
	int classCount = 0;

	while (true)
	{
		cout << "New class: ";
		string course;
		getline(cin, course);

		if (classCount < MAX_CLASSES)
		{
			classes[classCount] = course;
			classCount++;
		}
		else
		{
			cout << "The class list is full!" << endl << endl;
		}

	}

	return 0;
}
