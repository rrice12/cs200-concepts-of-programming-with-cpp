#include <iostream>
#include <string>
using namespace std;

int main()
{
	const int DEPARTMENTS = 3;
	const int EMPLOYEES = 2;

	const string DEPARTMENT_NAMES[3] = { "Software Development", "Quality Assurance", "Business Analyst" };

	string employees[DEPARTMENTS][EMPLOYEES];

	for (int i = 0; i < DEPARTMENTS; i++)
	{
		for (int j = 0; j < EMPLOYEES; j++)
		{
			cout << "Please enter employee #" << j << " from department " << DEPARTMENT_NAMES[i] << endl;
			getline(cin, employees[i][j]);
		}
	}


	while (true);
	return 0;
}
