#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include <string>
using namespace std;


void FillArray( int arr[], int size )
{
    for ( int i = 0; i < size; i++ )
    {
        arr[i] = rand() % 100;
    }
}

void PrintArray( int arr[], int size )
{
    cout << left <<  setw( 10 ) << "INDEX";
    for ( int i = 0; i < size; i++ )
    {
        cout << left << setw( 6 ) << i;
    }
    cout << endl << left << setw( 10 ) << "ELEMENT";
    for ( int i = 0; i < size; i++ )
    {
        cout << left << setw( 6 ) << arr[i];
    }
    cout << endl << endl;
}

void CopyArray( int from[], int to[], int size )
{
    for ( int i = 0; i < size; i++ )
    {
        to[i] = from[i];
    }
}

void Swap( int & a, int & b )
{
    int c = a;
    a = b;
    b = c;
}

void InsertionSort( int arr[], int size )
{
    int i = 1;
    while ( i < size )
    {
        int j = i;
        while ( j > 0 && arr[j-1] > arr[j] )
        {
            Swap( arr[j], arr[j-1] );
            j = j-1;
        }

        i = i + 1;
    }
}

void SelectionSort( int arr[], int size )
{
    for ( int i = 0; i < size-1; i++ )
    {
        int minIndex = i;

        for ( int j = i+1; j < size; j++ )
        {
            if ( arr[j] < arr[minIndex] )
            {
                minIndex = j;
            }
        }

        if ( minIndex != i )
        {
            Swap( arr[i], arr[minIndex] );
        }
    }
}

int LinearSearch( int findme, int arr[], int size )
{
    for ( int i = 0; i < size; i++ )
    {
        if ( arr[i] == findme )
        {
            return i;
        }
    }

    return -1; // not found
}

int BinarySearch( int findme, int arr[], int size )
{
    int left = 0;
    int right = size - 1;

    while ( left <= right )
    {
        int mid = ( left + right ) / 2;

        cout << "L=" << left << ", R=" << right << ", M=" << mid << endl;

        if ( arr[mid] < findme )
        {
            left = mid + 1;
            cout << "\t L=" << left << endl;
        }
        else if ( arr[mid] > findme )
        {
            right = mid - 1;
            cout << "\t R=" << right << endl;
        }
        else if ( arr[mid] == findme )
        {
            return mid;
        }
    }

    return -1; // not found
}

int main()
{
    srand( time( NULL ) );

    const int ARR_SIZE = 12;//10;
    int arr[ARR_SIZE] = { 10, 13, 15, 17, 18, 20, 22, 24, 25, 27, 29, 30 };
    //FillArray( arr, ARR_SIZE );

    int arr_insertion[ARR_SIZE];
    int arr_selection[ARR_SIZE];
    CopyArray( arr, arr_insertion, ARR_SIZE );
    CopyArray( arr, arr_selection, ARR_SIZE );

    cout << "Created array..." << endl;
    PrintArray( arr, ARR_SIZE );

    cout << "After insertion sort..." << endl;
    InsertionSort( arr_insertion, ARR_SIZE );
    PrintArray( arr_insertion, ARR_SIZE );

    cout << "After selection sort..." << endl;
    SelectionSort( arr_selection, ARR_SIZE );
    PrintArray( arr_selection, ARR_SIZE );

    cout << "Binary search..." << endl;
    int index = BinarySearch( 666, arr, ARR_SIZE );
    cout << "Found index: " << index << endl;

    return 0;
}
