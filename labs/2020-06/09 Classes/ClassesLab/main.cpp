#include <cstdlib>
#include <ctime>

#include <iostream>
using namespace std;

#include "Die.hpp"
#include "Student.hpp"

int main()
{
    srand( time( NULL ) );

    bool done = false;

    while ( !done )
    {

        cout << "Run program: ";
        int program;
        cin >> program;

        if ( program == 1 )
        {
            cout << "DIE PROGRAM" << endl;

            Die d20( 20 );
            Die d8( 8 );

            int hitRoll = d20.Roll();
            int damageRoll = d8.Roll();

            cout << "Hit roll: " << hitRoll << ", Damage roll: " << damageRoll << endl;
        }
        else if ( program == 2 )
        {
            cout << "STUDENT PROGRAM" << endl;

            Student student;

            cout << "Enter student name: ";
            string name;
            cin >> name;

            student.Setup( name );

            for ( int i = 0; i < 10; i++ )
            {
                cout << "Enter score " << i << ": ";
                int score;
                cin >> score;
                student.AddGrade( score );

                if ( score == -1 )
                {
                    break;
                }
            }

            student.Display();
        }
        else
        {
            done = true;
        }

    }

    return 0;
}
