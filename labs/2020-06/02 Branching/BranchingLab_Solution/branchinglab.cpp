#include <iostream>   // Use cin and cout
#include <string>     // Use strings
using namespace std;

void Program1()
{
    bool done = false;
    while ( !done )
    {
        cout << "Hometown" << endl;
        // .etc.

        cout << "Run again? (y/n): ";
        char choice;
        cin >> choice;

        if ( choice == 'n' )
        {
            done = true;
        }
    }
}

void Program2()
{
}

void Program3()
{
}

void Program4()
{
}

void Program5()
{
    float num1, num2, result;
    char operation;

    cout << "Enter first number: ";
    cin >> num1;

    cout << "Enter second number: ";
    cin >> num2;

    cout << "What type of operation?" << endl
        << " +: add" << endl
        << " -: subtract" << endl
        << " *: multiply" << endl
        << " /: divide" << endl << endl;

    cout << "Choice: ";
    cin >> operation;

    switch( operation )
    {
        case '+':
        result = num1 + num2;
        break;

        case '-':
        result = num1 - num2;
        break;

        case '*':
        result = num1 * num2;
        break;

        case '/':
//        if ( num2 == 0 ) { cout << "Can't divide by 0!" << endl; }
//        else { result = num1 / num2; }
        result = num1 / num2;
        break;

        default:
        cout << "Invalid operation!" << endl;
    }

    cout << endl << "Result: " << result << endl;
}

// Don't modify main
int main()
{
    while ( true )
    {
        cout << "Run which program? (1-5): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
