\contentsline {section}{\numberline {0.1}Using Visual Studio}{4}
\contentsline {subsection}{\numberline {0.1.1}Downloading Visual Studio}{4}
\contentsline {subsection}{\numberline {0.1.2}Installing Visual Studio}{5}
\contentsline {subsection}{\numberline {0.1.3}Creating a program}{6}
\contentsline {subsection}{\numberline {0.1.4}Writing a program}{9}
\contentsline {subsection}{\numberline {0.1.5}Building and running the program}{10}
\contentsline {subsection}{\numberline {0.1.6}Locating your source file}{12}
\contentsline {section}{\numberline {0.2}Using Code::Blocks}{14}
\contentsline {subsection}{\numberline {0.2.1}Downloading Code::Blocks}{14}
\contentsline {subsection}{\numberline {0.2.2}Installing Code::Blocks}{15}
\contentsline {subsection}{\numberline {0.2.3}Creating a program}{15}
\contentsline {paragraph}{Project type selection screen:}{16}
\contentsline {paragraph}{Project information screen:}{16}
\contentsline {paragraph}{Compiler information screen:}{17}
\contentsline {subsection}{\numberline {0.2.4}Writing a program}{20}
\contentsline {subsection}{\numberline {0.2.5}Building and running the program}{21}
\contentsline {subsection}{\numberline {0.2.6}Locating your source file}{23}
