#ifndef _PARSER_HPP
#define _PARSER_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

#include "timer.hpp"
#include "animal.hpp"

vector<string> Split( string line, string delimiter );
void LoadCsvData( const string& filepath, vector<Animal>& animals );

#endif
