#include "AccountManager.hpp"

#include <iostream>
using namespace std;

AccountManager::AccountManager()
{
}

AccountManager::~AccountManager()
{
}

void AccountManager::AddAccount( string username, string plaintextPassword )
{
}

bool AccountManager::UsernameExists( string username ) const
{
    return false; // placeholder
}

bool AccountManager::ValidatePassword( string username, string plaintextPassword )
{
    return false; // placeholder
}

int AccountManager::GetIndexOfUser( string username )
{
    return -1; // placeholder
}

bool AccountManager::SignIn( string username, string plaintextPassword )
{
    return false; // placeholder
}

string AccountManager::GetLoggedInUsername() const
{
    return ""; // placeholder
}


/* Already written functions **********************************************/
void AccountManager::Load()
{
    ifstream input( "accounts.dat" );
    if ( input.fail() )
    {
        return;
    }

    string username;
    size_t password;

    while ( input >> username >> password )
    {
        Account account( username, password );
        m_accounts.push_back( account );
    }

    cout << m_accounts.size() << " accounts loaded." << endl;
}

void AccountManager::Save()
{
    ofstream output( "accounts.dat" );
    for ( auto& account : m_accounts )
    {
        output << account.GetUsername() << " " << account.GetHashedPassword() << endl;
    }
    output.close();
}

size_t AccountManager::HashPassword( string plaintextPassword )
{
    hash<string> stringHash;
    return stringHash( plaintextPassword );
}
