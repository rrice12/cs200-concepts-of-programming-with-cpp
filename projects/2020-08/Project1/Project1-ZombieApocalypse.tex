\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {Zombie Apocalypse}
\newcommand{\laTitle}       {CS 200 Project 1}
\newcounter{question}

\renewcommand{\chaptername}{Topic}

\usepackage{../../../rachwidgets}
\usepackage{../../../rachdiagrams}

\title{}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTopic \ / \laTitle}

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}

\setcounter{chapter}{1}
\chapter*{Project 1: Zombies}

	\begin{center}

	\end{center}

\begin{figure}[h]
    \centering
    \begin{subfigure}{.6\textwidth}

		\section{Assignment info}

		\begin{itemize}
			\item	Once finished with the project, upload any source files for your project - .hpp, .h, and/or .cpp.
					~\\ \textbf{Please don't zip your code.}
			\item	This project is \textbf{ \underline{solo effort} } - you can ask the instructor for help,
					but otherwise should be working on this project solo. Don't give code to or receive code from
					other people or locations.
			\item	Feel free to modify the program to add new features or, if you have prior programming experience,
					cleaning up the code by using functions.
			\item	This project covers variables, input, output, branching, and loops.
		\end{itemize}

    \end{subfigure}%
    \begin{subfigure}{.4\textwidth}

        \centering
        \includegraphics[width=5cm]{images/zombie-cartridge.png}

    \end{subfigure}
\end{figure}

	\begin{itemize}
		\item	Since this is the first project, this document will cover
				a lot of what you need to complete this project.
		\item	Make sure to save and build regularly to fix compile errors
				as they pop up! And test regularly as well. :)
	\end{itemize}

\tableofcontents


\newpage
\section{Grading breakdown}

\begin{center}
	%\includegraphics[width=15cm]{Project1-Grading-Breakdown.png}
\end{center}

\newpage

	\section{Starting the Zombie Apocalypse}

	\paragraph{About the game:}
	In this game, the user will have to keep track of how much \underline{food}
	they have and what their \underline{health} is, and decide whether to
	stay at a location to \underline{scavenge} for supplies or to \underline{travel}
	to another location. During these events (scavenging or traveling), a random
	event may occur that affects the player's food or health.

\begin{lstlisting}[style=output]
----------------------------------------
-- Naught A. Zombee                   --
-- Day 1   Food: 5   Health: 20/20    --
-- Location: Overland Park            --
----------------------------------------
-- 1. Scavenge here                   --
-- 2. Travel elseware                 --
----------------------------------------
\end{lstlisting}

	The game will keep track of how many days have passed, and if the player
	survives for 20 days, they will ``win'' the game. If their health falls to
	0 or below, they will lose the game.

	\paragraph{Implementing the game in steps:} This document will split up building
	the game into different \textbf{iterations} - implementing one or two
	features at a time and testing as we go.

	Software projects are often large and cannot just be built from
	the ground-up 100\% complete as-is. The software architects
	take the software's requirements and breaks it up into smaller
	chunks, sets of features, to be implemented a few at a time.
	This project's design doc is presented iteratively, so you're implementing
	a few features at a time, giving you the ability to build, run, and test
	for each iteration as you go.

	\paragraph{Starter code:} There is also starter code provided to begin
	with, which you can download off Canvas or type in manually (the Appendix
	of this document contains the starter code).



	\newpage
	\section{Programming the game}

		%--------------------------------------------------------------%
		%--------------------------------------------------------------%
		%--------------------------------------------------------------%
		\subsection{Iteration 1: Starter code}							%----%

		The starter code already has all of our variables declared and the
		start of the game loop. There is also a \textbf{function} called
		\texttt{DisplayMenu} already written - you don't have to modify
		this at all, and the function is already called for you. It is just
		there to keep the code a little cleaner. Most of the work you
		will do is within the while loop.

		\paragraph{Variables:}
		For this iteration, familiarize yourself with the variables I've declared.

		~\\
		\begin{tabular}{p{3cm} p{2cm} p{7.5cm}}
			\textbf{Variable} 	& \textbf{Type} 	& \textbf{Description} \\ \hline
			done				& bool					& Flag for if the game is over.	\\ \hline
			successfulAction	& bool					& Flag for if the user entered valid input.	\\ \hline
			food				& int					& Count of how much food the player has.	\\ \hline
			maxHealth			& int					& The maximum amount of health the player can have.	\\ \hline
			health				& int					& The current amount of health the player has.	\\ \hline
			name				& string				& The name of the player's character.	\\ \hline
			location			& string				& The name of the location the player is at.	\\ \hline
			day					& int					& How many days have gone by.	\\ \hline
			dump				& string				& Used to store string input temporarily.	\\ \hline
			choice				& int					& Used to store menu input.	\\ \hline
			mod					& int					& Used for calculations	\\
		\end{tabular}

		\begin{hint}{C++ naming conventions}
			In C++, is it standard to give variables camel case names, with the
			first letter being lower-case, like this:

			\texttt{playerChoice}, \texttt{howManyBugsInABox}, \texttt{secretOfMonkeyIsland}
		\end{hint}

		\newpage
		\paragraph{Game setup code:}
		There are a few things that happen prior to the game loop starting.

			\subparagraph{Seeding the random number generator} ~\\
\begin{lstlisting}[style=code]
srand( time( NULL ) );
\end{lstlisting}

			\begin{intro}{About random numbers and seeds}
				Computers can't actually generate random numbers. When we use the
				\texttt{rand()} function in C++ (or other similar function in other languages),
				it will generate a string of numbers that \textit{appear} random, but aren't.
				If your an the program several times, you would always get the same string of
				``random'' numbers.

				\begin{center}
					\texttt{1804289383
					846930886
					1681692777
					1714636915
					1957747793}
				\end{center}

				This is why we \textbf{seed} our random number generators. Different seeds
				will give us different sequences of ``random'' numbers, though if we always
				seeded with the same number, we would still get the same ``random'' sequence
				over and over again.
				Because of this, a common practice is to \textbf{seed the random number generator with the time.}

				\begin{center}
					\texttt{ srand( time( NULL ) ); }
				\end{center}

				It is unlikely that you will launch the program more than once for any given second.
				The \texttt{time()} function returns the amount of seconds that have ellapsed since
				January 1st, 1970 (the current unix timestamp). This gives us a more
				``random'' sequence of random numbers every time we start our program.
		\end{intro}

		\subparagraph{Displaying the title and getting the player's name} ~\\
\begin{lstlisting}[style=code]
cout << "Z O M B I E  -  A P O C A L Y P S E" << endl;
cout << "Enter your name: ";
getline( cin, name );
\end{lstlisting}

		This is just when the game starts up. After the player enters
		their name, the game loop will begin, and it will continue running
		until the user either quits or wins or loses the game.

		\subparagraph{The game loop} ~\\
\begin{lstlisting}[style=code]
while ( !done )
{
	/** Start of the round **/
	successfulAction = false;
	DisplayMenu( day, food, health, maxHealth, name, location );
	cout << "CHOICE: ";
	cin >> choice;

	/** Student implements features here **/

	/** End of the round - nothing to update **/
	cout << endl << "Press ENTER to continue...";
	cin.ignore();
	getline( cin, dump );

	cout << "----------------------------" << endl;
	cout << "----------------------------" << endl;
}
\end{lstlisting}

		The game loop has been started for you, and you will implement
		features outlined in this document at the point where it says

		\begin{center}
		\texttt{ /** Student implements features here **/ }
		\end{center}

		Before that, it makes a call to the \texttt{DisplayMenu} function
		(already implemented) that will display a nice looking header
		at the start of each round:

\begin{lstlisting}[style=output]
----------------------------------------
-- Naught A. Zombee                   --
-- Day 1   Food: 5   Health: 20/20    --
-- Location: Overland Park            --
----------------------------------------
-- 1. Scavenge here                   --
-- 2. Travel elseware                 --
----------------------------------------
\end{lstlisting}

		and also gets the user's choice for if they want to scavenge
		or travel, which will be stored in the \texttt{choice} variable.
		~\\

		Before the \textit{end} of the loop, it will pause the game,
		asking the user to press ENTER to continue. When they do, the next
		round will begin and the loop continues.

		Even though you're not adding any code for iteration 1, run
		the starter code and make sure everything builds and runs properly.






		%--------------------------------------------------------------%
		%--------------------------------------------------------------%
		%--------------------------------------------------------------%
		\hrulefill
		\subsection{Iteration 2: Daily events}							%----%

		At the end of each round (before the ``End of the round'' comment),
		the player's stats will be changed.

\begin{lstlisting}[style=output]
* The day passes (+1 day).
* You eat a meal (-1 food).
\end{lstlisting}

		A day will pass, the player's
		character will eat one unit of food if they have food. If the player
		doesn't have food, they will lose some health. If their health reaches 0,
		they will die. If they're able to survive until day 20, they will win.

		Implement each of the following...

		\paragraph{A day passes:}
		Display a message like ``* The day passes (+1 day).'', and increment the \texttt{day} variable by 1.

		\paragraph{Eating food... or not:} Use an if/else statement. The condition
		for the if statement should be ``is the amount of \texttt{food} greater than 0?''

		\begin{itemize}
			\item	\texttt{ if ( food > 0 ) } is true...
				\begin{itemize}
					\item	Output a message like ``* You eat a meal (-1 food).''
					\item	Decrement the \texttt{food} variable by 1.
					\item	Increment the \texttt{health} variable by 1.
				\end{itemize}
			\item	\texttt{else}...
				\begin{itemize}
					\item	Set the \texttt{mod} variable to a random number between 1 and 4.
							~\\ (see next page)
					\item	Output a message, ``* You are starving! (-AMOUNT health)''.
							~\\ (Replace ``AMOUNT'' with the value of \texttt{mod}.)
							~\\ \texttt{ cout << "(-" << mod << " health)" << endl; }
					\item	Subtract \texttt{mod} from \texttt{health} and store it as \texttt{health}'s new value.
							~\\ (see next page)
				\end{itemize}
		\end{itemize}

		More info on the next page on how to do these things. Note that I'm generally
		not giving you the exact code, like you wouldn't put ``is true...'' at the end of
		your if statement.

		\begin{hint}{Variables and math}
			An assignment statement for a variable looks like this:
			\begin{center}
				\texttt{ variableName = value; }
			\end{center}

			The \underline{variable being assigned to} is placed on the \textbf{left-hand side}
			(sometimes written as ``LHS'') of the equal sign, and the \underline{value}
			it is receiving is on the \textbf{right-hand side} (RHS) of the equal sign.

			~\\
			The \underline{value} can be a hard-coded number, another variable, or a math expression.

			~\\
			You can also use a variable's existing value as part of that value expression:
			\begin{center}
				\texttt{ variableName = variableName - 5; }
			\end{center}

			Updating a variable based on its current value is common, so there are also
			shorthand ways to code the same thing:
			\begin{center}
				\begin{tabular}{p{5cm} p{7cm}}
					\texttt{ variableName -= 5; }   & Subtracts 5 from the variable and stores it back in the variable.
					\\ \\
					\texttt{ variableName += 5; } 	& Adds 5 to the variable and stores it back in the variable.
				\end{tabular}
			\end{center}

			Adding and subtracting 1 from a variable is common, so there are also
			shortcuts in C++ to add or subtract exactly one item:
			\begin{center}
				\begin{tabular}{p{4cm} p{8cm}}
					\texttt{ variableName++; }  & Adds 1 to the variable after the statement.
					\\ \\
					\texttt{ ++variableName; } 	& Adds 1 to the variable before the statement.
					\\ \\
					\texttt{ variableName--; } 	& Subtracts 1 from the variable after the statement.
					\\ \\
					\texttt{ --variableName; } 	& Subtracts 1 from the variable before the statement.
				\end{tabular}
			\end{center}
		\end{hint}

		\newpage
		\begin{hint}{Random numbers}
			The \texttt{rand()} function will give you a big random number.
			To keep that number bound, you can use the \textbf{modulus operator \%}
			to make sure it stays between 0 and $n-1$ (whatever you did the modulus by)...

			\begin{center}
				\texttt{ mod = rand() \% 4; } ~\\

				Will store 0, 1, 2, or 3 into \texttt{mod}.
			\end{center}

			If you want to set a lower-bound, you can add a minimum value onto
			the end of this statement - you will still get four results, but
			everything will be offset by that starter value.

			\begin{center}
				\texttt{ mod = rand() \% 4 + 1; } ~\\

				Will store 1, 2, 3, or 4 into \texttt{mod}.

				~\\
				\texttt{ mod = rand() \% 4 + 2; } ~\\

				Will store 2, 3, 4, or 5 into \texttt{mod}.
			\end{center}
		\end{hint}

		\begin{center}
			(By the way, this interlude would be a good chance to compile
			your code and make sure there are no syntax errors!)
		\end{center}

		\paragraph{Bind health to a max of 20:} It is possible for the player
		to be healed over a value of 20 (well, not \textit{now} but it will
		be once we implement more in the game!), so we want to make sure that
		the \texttt{health} variable doesn't go above \texttt{maxHealth}.

		An easy way to solve this is: if \texttt{health} is more than \texttt{maxHealth},
		then write an assignment statement, setting \texttt{health}'s value to
		\texttt{maxHealth}.

		\paragraph{Bind food to a minimum of 0:} Likewise, we don't want food
		to drop to negative numbers; we want it to stay at 0. (Otherwise, that
		would be food debt, and debt is terrible. Let's pretend debt no longer
		exists in this zombie apocalypse!)

		We can solve this similarly to the health: If \texttt{food} is less than
		0, then set \texttt{food} to 0.

\begin{lstlisting}[style=code]
if ( food <= 0 )
{
	food = 0;
}
\end{lstlisting}

		\newpage
		\paragraph{Game over state - health is 0:}
		For this, we will check to see if \texttt{health} is 0 or less.
		If that's true, then display a message like ``* You have died.''
		and then set the \texttt{done} variable to \texttt{true}.

		\paragraph{You win state - day is 20:}
		Write this as an ``else if'' statement attached to the above check.
		We're adding this on because it's possible for the player to hit 0
		health on the 20th day, and they can't win if they're dead.
		\footnote{Insert nihilistic millennial joke here.}

		Within this else if check, display a message like:

		\begin{center}
			``In the morning, a group of scavengers find you.
			They have a fortification nearby and are rebuilding a society.
			You agree to live in their town.''
		\end{center}

		(or write whatever you think would be an appropriate good ending)
		and then set \texttt{done} to \texttt{true}.

		\subsubsection{Testing!}

		Before continuing on, build and run your program. Although
		selecting the menu options don't do anything, you will see the
		stat updates.

\begin{lstlisting}[style=output]
* The day passes (+1 day).
* You eat a meal (-1 food).
\end{lstlisting}

\begin{lstlisting}[style=output]
* The day passes (+1 day).
* You have died.

Press ENTER to continue...
----------------------------------------
----------------------------------------

You survived the apocalpyse on your own for 15 days.
\end{lstlisting}

		If you want to test if your win state works, you could temporarily
		comment out any code that subtracts from player health, but make
		sure to re-enable it before continuing on!

		%--------------------------------------------------------------%
		%--------------------------------------------------------------%
		%--------------------------------------------------------------%
		\newpage
		\subsection{Iteration 3: Scavenging}

		Next we'll implement scavenging, for when the player selects menu
		option \#1 from the main menu. First start off by writing
		an if statement or switch statement to check if \texttt{choice} is equal to 1.

		\begin{hint}{Don't mix it up with an assignment statement!}
		Remember that to check if two things are equal, your equal sign needs a sequel:

			\begin{center}
				\texttt{if ( a == b )}		\tab 	Are a and b equal? ~\\ ~\\
				\texttt{a = b;}		\tab 	Assign b to a.
			\end{center}
		\end{hint}

		\begin{hint}{Switch statements and declaring variables}
			If you write a switch statement like this:

\begin{lstlisting}[style=code]
switch( choice )
{
	case 1:
	break;
	
	// ...
}
\end{lstlisting}

			And declare a variable somewhere within a case statement,
			you will get a compiler error. To get around this, surround
			the case code within curly braces \texttt{ \{ \} }:

\begin{lstlisting}[style=code]
switch( choice )
{
	case 1:
	{
		int randomChoice;
	}
	break;
	
	// ...
}
\end{lstlisting}

		\end{hint}
		
		\newpage
		Within this if statement, display a message like ``* You scavenge here.''. 
		Also set the \texttt{successfulAction} variable to \texttt{true} (we will use it
		next iteration.) 		
		Then, we're going to generate a random event.

		Create an integer variable called \texttt{randomChance}, and assign
		it a random value between 0 and 4.

\begin{lstlisting}[style=code]
int randomChance = rand() % 5;
\end{lstlisting}

		Then create another if statement or switch statement that will handle
		for \texttt{randomChance} being 0, 1, 2, 3, or 4:
		
		\paragraph{Event 0: Find food} ~\\
\begin{lstlisting}[style=output]
* You scavenge here.
* You find a stash of food. (+4 food)
\end{lstlisting}

			\begin{enumerate}
				\item	Assign a \textbf{random number} between 2 and 5 and store the result in the \texttt{mod} variable.
				\item	Display a message like ``* You find a stash of food. (+AMOUNT food)''
						~\\ (Replace AMOUNT with the value of \texttt{mod}.)
						~\\ \texttt{ cout << "(+" << mod << " food)" << endl; }
				\item	Add \texttt{mod} to \texttt{food}.
						~\\ \texttt{ food += mod; }
			\end{enumerate}
		

		\paragraph{Event 1: Surprised by zombie} ~\\
\begin{lstlisting}[style=output]
* You scavenge here.
* A zombie surprises you!
* You get hurt in the encounter. (-2 health)
\end{lstlisting}

			\begin{enumerate}
				\item	Assign a \textbf{random number} between 2 and 8 and store the result in the \texttt{mod} variable.
				\item	Display a message like ``* A zombie surprises you! You get hurt in the encounter. (-AMOUNT health)''
						~\\ (Replace AMOUNT with the value of \texttt{mod}.)
				\item	Subtract \texttt{mod} from \texttt{health}.
			\end{enumerate}
		
		\newpage
		\paragraph{Event 2: Find medical supplies} ~\\
\begin{lstlisting}[style=output]
* You scavenge here.
* You find some medical supplies. (+3 health)
\end{lstlisting}
		
			\begin{enumerate}
				\item	Assign a \textbf{random number} between 2 and 5 and store the result in the \texttt{mod} variable.
				\item	Display a message like ``* You find some medical supplies. (+AMOUNT health)''
						~\\ (Replace AMOUNT with the value of \texttt{mod}.)
				\item	Add \texttt{mod} to \texttt{health}.
			\end{enumerate}
			
		\paragraph{Event 3: Ambushed} ~\\
\begin{lstlisting}[style=output]
* You scavenge here.
* Another scavenger ambushes you!
* They take some supplies from you. (-5 food)
\end{lstlisting}
		
			\begin{enumerate}
				\item	Assign a \textbf{random number} between 2 and 6 and store the result in the \texttt{mod} variable.
				\item	Display a message like ``* Another scavenger ambushes you! * They take some supplies from you. (+AMOUNT food)''
						~\\ (Replace AMOUNT with the value of \texttt{mod}.)
				\item	Subtract \texttt{mod} from \texttt{food}.
			\end{enumerate}
		
		\paragraph{Event 4: Nothing} ~\\
\begin{lstlisting}[style=output]
* You scavenge here.
* You don't find anything.
\end{lstlisting}
		
			\begin{enumerate}
				\item	Display a message like ``* You don't find anything.''
			\end{enumerate}


		\subsubsection{Testing iteration 4}
		Make sure to build, run, and test the program at this point! Look
		for updates to the variables, and make sure you get each of the five
		random events.


		%--------------------------------------------------------------%
		%--------------------------------------------------------------%
		%--------------------------------------------------------------%
		\newpage
		\subsection{Iteration 4: Moving}

		This will be an \textbf{else if} statement (or a second switch case);
		if the user's choice wasn't 1, now we're checking if it was 2 and
		executing this action.
		
		First off, ask the player where they want to go with a numbered list:
		
\begin{lstlisting}[style=output]
Walk to where?
1. Overland Park
2. Raytown
3. Kansas City
4. Gladstone
\end{lstlisting}

		Get their input - you can store it in the \texttt{choice} variable again.
		
		Next, we need to figure out if their selection is valid - did they
		choose 1, 2, 3, or 4? Also, did they choose a location that is
		\textit{different} from where they're currently at (which is stored in the
		\texttt{location} variable)?
		
		There are different ways you can organize this logic with if statements,
		nested if statements, and logic operators, but here's the different criterias:
		
		\begin{itemize}
			\item	\texttt{choice} is 1 and \texttt{location} is \texttt{"Overland Park"}: ~\\
					Display a message, ``You're already there!''
			\item	\texttt{choice} is 1 and \texttt{location} is NOT \texttt{"Overland Park"}: ~\\
					Set \texttt{location} to \texttt{"Overland Park"} ~\\
					Set \texttt{successfulAction} to \texttt{true}
					
					\hrulefill
					
			\item	\texttt{choice} is 2 and \texttt{location} is \texttt{"Raytown"}: ~\\
					Display a message, ``You're already there!''
			\item	\texttt{choice} is 2 and \texttt{location} is NOT \texttt{"ORaytown"}: ~\\
					Set \texttt{location} to \texttt{"Raytown"} ~\\
					Set \texttt{successfulAction} to \texttt{true}
					
					\hrulefill
					
			\item	\texttt{choice} is 3 and \texttt{location} is \texttt{"Kansas City"}: ~\\
					Display a message, ``You're already there!''
			\item	\texttt{choice} is 3 and \texttt{location} is NOT \texttt{"Kansas City"}: ~\\
					Set \texttt{location} to \texttt{"Kansas City"} ~\\
					Set \texttt{successfulAction} to \texttt{true}
					
					\hrulefill
					
			\item	\texttt{choice} is 4 and \texttt{location} is \texttt{"Gladstone"}: ~\\
					Display a message, ``You're already there!''
			\item	\texttt{choice} is 4 and \texttt{location} is NOT \texttt{"Gladstone"}: ~\\
					Set \texttt{location} to \texttt{"Gladstone"} ~\\
					Set \texttt{successfulAction} to \texttt{true}
					
					\hrulefill
					
			\item	else/default case: ~\\
					Display ``Invalid selection!''
		\end{itemize}
		
		Once you've either displayed an error or determined a new location,
		we are going to trigger another random event. Note that,
		if the user selected a \textit{valid location}, then
		\texttt{successfulAction} is now set to \texttt{true}.
		If something was invalid, it will be set to \texttt{false} and
		no random even will happen because their location didn't change.
		~\\
		
		Add a check for \texttt{ if ( successfulAction ) },
		and add the following logic within this if statement.
		
		Display a message like ``You travel to LOCATION.'', replacing
		LOCATION with \texttt{location}. Then generate a random
		number between 0 and 4 (\texttt{rand() \% 5}) and store it
		in an integer variable named \texttt{randomChance}.
		
		Use a set of if/else if statements or a switch statement
		to make updates for each random event:
		
		\paragraph{Event 0: Zombie fight 1} ~\\
\begin{lstlisting}[style=output]
* A zombie attacks!
* You fight it off.
\end{lstlisting}

			\begin{enumerate}
				\item	Just display a message like ``* A zombie attacks! You fight it off.''
			\end{enumerate}
		
		\newpage
		\paragraph{Event 1: Zombie fight 2} ~\\
\begin{lstlisting}[style=output]
* A zombie attacks!
* It bites you as you fight it! (-4 health)
\end{lstlisting}
		
			\begin{enumerate}
				\item	Assign a \textbf{random number} between 2 and 6 and store the result in the \texttt{mod} variable.
				\item	Display a message like ``* A zombie attacks! It bites you as you fight it! (-AMOUNT health)''
						~\\ (Replace AMOUNT with the value of \texttt{mod}.)
				\item	Subtract \texttt{mod} from \texttt{health}.
			\end{enumerate}
			
		\paragraph{Event 2: Trade} ~\\
\begin{lstlisting}[style=output]
* You find another scavenger and trade goods. (+2 food)
\end{lstlisting}
		
			\begin{enumerate}
				\item	Assign a \textbf{random number} between 2 and 4 and store the result in the \texttt{mod} variable.
				\item	Display a message like ``* You find another scavenger and trade goods. (+AMOUNT food)''
						~\\ (Replace AMOUNT with the value of \texttt{mod}.)
				\item	Add \texttt{mod} to \texttt{food}.
			\end{enumerate}
			
		\paragraph{Event 3: Safe house} ~\\
\begin{lstlisting}[style=output]
* You find a safe building to rest in temporarily. (+3 health)
\end{lstlisting}
		
			\begin{enumerate}
				\item	Assign a \textbf{random number} between 2 and 5 and store the result in the \texttt{mod} variable.
				\item	Display a message like ``* You find a safe building to rest in temporarily. (+AMOUNT health)''
						~\\ (Replace AMOUNT with the value of \texttt{mod}.)
				\item	Add \texttt{mod} to \texttt{health}.
			\end{enumerate}
		
		\paragraph{Event 4: Uneventful} ~\\
\begin{lstlisting}[style=output]
* The journey is uneventful.
\end{lstlisting}
		
			\begin{enumerate}
				\item	Display a message like ``* The journey is uneventful.''
			\end{enumerate}
		
		\vspace{2cm}
		
		Afterwards, add an \textbf{else} case, where \texttt{successfulAction} was false.
		In this case, use the \texttt{continue} command. This will keep the rest of the loop from
		executing (adding to a day, removing food) because the user didn't enter a valid command.

\begin{lstlisting}[style=code]
if ( successfulAction )
{
	// Stuff you did
}
else
{
	continue;
}
\end{lstlisting}

		\subsection{Testing the game}
		Build and run the game and make sure the stat variables get updated
		for each event type. Also make sure you can get to the game over state
		and the you win state.


		%--------------------------------------------------------------%
		%--------------------------------------------------------------%
		%--------------------------------------------------------------%
\chapter{Appendix: Starter code}

\paragraph{main.cpp} ~\\

\begin{lstlisting}[style=codesmall]
#include <iostream>     // Used for cin and cout
#include <iomanip>      // Used for formatting cout statements
#include <string>       // Used for string data types
#include <cstdlib>      // Used for rand()
#include <ctime>        // time used to seed random number generator
using namespace std;

void DisplayMenu( int day, int food, int health, int maxHealth, string name, string location );

int main()
{
    /******************************************************************* INITIALIZE VARIABLES */
    bool done = false;                      // Flag for if the game is over
    bool successfulAction = false;          // Flag for if the user entered valid input
    int food = 5;                           // Player stat - how much food they have
    int maxHealth = 20;                     // Player stat - their maximum amount of health
    int health = maxHealth;                 // Player stat - their current health amount
    string name = "ME";                     // Player stat - what their name is
    string location = "Overland Park";      // Player stat - what location they're in
    int day = 1;                            // Player stat - how many days it has been

    string dump;                            // A variable to dump input into
    int choice;                             // A variable to dump input into
    int mod;                                // A variable used for calculations later

    srand( time( NULL ) );                  // Seeding the random number generator

    /******************************************************************* GAME START */
    cout << "Z O M B I E  -  A P O C A L Y P S E" << endl;
    cout << "Enter your name: ";
    getline( cin, name );

    cout << endl;

    /******************************************************************* GAME LOOP */
    while ( !done )
    {
        /** Start of the round **/
        DisplayMenu( day, food, health, maxHealth, name, location );
        cout << "CHOICE: ";
        cin >> choice;

        /** Student implements features here **/

        /** End of the round - nothing to update **/
        cout << endl << "Press ENTER to continue...";
        cin.ignore();
        getline( cin, dump );

        cout << "----------------------------------------" << endl;
        cout << "----------------------------------------" << endl;
    }

    /******************************************************************* GAME OVER */
    cout << endl << endl << "You survived the apocalpyse on your own for " << day << "days." << endl;

    return 0;
}

/* You don't need to modify this, it just displays the user's information at the start of each round. */
void DisplayMenu( int day, int food, int health, int maxHealth, string name, string location )
{
    cout << "----------------------------------------" << endl;
    cout << left << setw( 3 ) << "-- " << setw( 35 ) << name << "--" << endl;
    cout << left
        << setw( 6 ) << "-- Day "   << setw( 4 ) << day
        << setw( 6 ) << "Food: "    << setw( 4 ) << food
        << setw( 8 ) << "Health: "  << setw( 2 ) << health << setw( 1 ) << "/" << setw( 2 ) << maxHealth
        << right << setw( 6 ) << "--" << endl;
    cout << left
        << setw( 10 ) << "-- Location: " << setw( 20 ) << location
        << right << setw( 7 ) << "--" << endl;
    cout << "----------------------------------------" << endl;
    cout << "-- 1. Scavenge here                   --" << endl;
    cout << "-- 2. Travel elseware                 --" << endl;
    cout << "----------------------------------------" << endl;
    for ( int i = 0; i < 15; i++ )
    {
        cout << endl;
    }
}
\end{lstlisting}




\end{document}

