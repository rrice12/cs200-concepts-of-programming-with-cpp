#ifndef _VECTOR_TESTER_HPP
#define _VECTOR_TESTER_HPP

#include <iostream>
#include <string>
using namespace std;

#include "TesterBase.hpp"
#include "StorageTester.hpp"
#include "../utilities/StringUtil.hpp"
#include "../utilities/Logger.hpp"

class StorageTester : public TesterBase
{
public:
    StorageTester( string filename = "test_result.html" )
        : TesterBase( filename )
    {
        AddTest(TestListItem("IsFull()",           bind(&StorageTester::Test_IsFull, this)));
        AddTest(TestListItem("IsValidIndex()",     bind(&StorageTester::Test_IsValidIndex, this)));
        AddTest(TestListItem("AddItem()",          bind(&StorageTester::Test_AddItem, this)));
        AddTest(TestListItem("UpdateItem()",       bind(&StorageTester::Test_UpdateItem, this)));
        AddTest(TestListItem("ClearAllItems()",    bind(&StorageTester::Test_ClearAllItems, this)));
        AddTest(TestListItem("SaveItems()",        bind(&StorageTester::Test_SaveItems, this)));
        AddTest(TestListItem("LoadItems()",        bind(&StorageTester::Test_LoadItems, this)));
    }

    virtual ~StorageTester() { }

private:
    int Test_IsFull();
    int Test_IsValidIndex();
    int Test_AddItem();
    int Test_UpdateItem();
    int Test_ClearAllItems();
    int Test_SaveItems();
    int Test_LoadItems();
};

int StorageTester::Test_IsFull()
{
    StartTestSet( "StorageTester::IsFull", { } );
    Logger::Out( "Beginning test", "StorageTester::IsFull" );

    { /* TEST BEGIN ************************************************************/
        StartTest( "Make sure IsFull returns false when not full" );

        bool expectedResult = false;
        bool actualResult   = IsFull( 3, 5 );

        Set_ExpectedOutput  ( "IsFull()", expectedResult );
        Set_ActualOutput    ( "IsFull()", actualResult );

        if ( expectedResult != actualResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "Make sure IsFull returns true when full" );

        bool expectedResult = true;
        bool actualResult   = IsFull( 5, 5 );

        Set_ExpectedOutput  ( "IsFull()", expectedResult );
        Set_ActualOutput    ( "IsFull()", actualResult );

        if ( expectedResult != actualResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int StorageTester::Test_IsValidIndex()
{
    StartTestSet( "StorageTester::IsValidIndex", { } );
    Logger::Out( "Beginning test", "StorageTester::IsValidIndex" );

    { /* TEST BEGIN ************************************************************/
        StartTest( "Check IsValidIndex for index -5" );

        bool expectedResult = false;
        bool actualResult   = IsValidIndex( -5, 3, 5 );

        Set_ExpectedOutput  ( "IsValidIndex()", expectedResult );
        Set_ActualOutput    ( "IsValidIndex()", actualResult );

        if ( expectedResult != actualResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "Check IsValidIndex for index 0 with itemCount=3, ARRAY_SIZE=5" );

        bool expectedResult = true;
        bool actualResult   = IsValidIndex( 0, 3, 5 );

        Set_ExpectedOutput  ( "IsValidIndex()", expectedResult );
        Set_ActualOutput    ( "IsValidIndex()", actualResult );

        if ( expectedResult != actualResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "Check IsValidIndex for index 3 with itemCount=3, ARRAY_SIZE=5" );

        bool expectedResult = false;
        bool actualResult   = IsValidIndex( 3, 3, 5 );

        Set_ExpectedOutput  ( "IsValidIndex()", expectedResult );
        Set_ActualOutput    ( "IsValidIndex()", actualResult );

        if ( expectedResult != actualResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "Check IsValidIndex for index 7 with itemCount=3, ARRAY_SIZE=5" );

        bool expectedResult = false;
        bool actualResult   = IsValidIndex( 7, 3, 5 );

        Set_ExpectedOutput  ( "IsValidIndex()", expectedResult );
        Set_ActualOutput    ( "IsValidIndex()", actualResult );

        if ( expectedResult != actualResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "Check IsValidIndex for index 2 with itemCount=3, ARRAY_SIZE=5" );

        bool expectedResult = true;
        bool actualResult   = IsValidIndex( 2, 3, 5 );

        Set_ExpectedOutput  ( "IsValidIndex()", expectedResult );
        Set_ActualOutput    ( "IsValidIndex()", actualResult );

        if ( expectedResult != actualResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/


    FinishTestSet();
    return TestResult();
}

int StorageTester::Test_AddItem()
{
    StartTestSet( "StorageTester::AddItem", { } );
    Logger::Out( "Beginning test", "StorageTester::AddItem" );

    { /* TEST BEGIN ************************************************************/
        StartTest( "Add item then make sure it's in the array" );

        const int AS = 5;
        int ic = 0;
        string arr[AS];

        AddItem( "ItemOne", arr, ic, AS );

        string expectedResult = "ItemOne";
        string actualResult = arr[0];

        Set_ExpectedOutput  ( "arr[0]", expectedResult );
        Set_ActualOutput    ( "arr[0]", actualResult );

        if ( actualResult != expectedResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "Add two items then make sure it's in the array" );

        const int AS = 5;
        int ic = 0;
        string arr[AS];

        AddItem( "ItemOne", arr, ic, AS );
        AddItem( "ItemTwo", arr, ic, AS );

        string expectedResult[] = { "ItemOne", "ItemTwo" };

        bool allMatch = true;
        for ( int i = 0; i < ic; i++ )
        {
            Set_ExpectedOutput  ( "arr[" + StringUtil::ToString( i ) + "]", expectedResult[i] );
            Set_ActualOutput    ( "arr[" + StringUtil::ToString( i ) + "]", arr[i] );

            if ( arr[i] != expectedResult[i] )
            {
                allMatch = false;
            }
        }

        if ( !allMatch )   { TestFail(); }
        else               { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "Add two items then make sure itemCount is correct" );

        const int AS = 5;
        int ic = 0;
        string arr[AS];

        AddItem( "ItemOne", arr, ic, AS );
        AddItem( "ItemTwo", arr, ic, AS );

        int expectedResult = 2;
        int actualResult = ic;

        Set_ExpectedOutput  ( "itemCount", expectedResult );
        Set_ActualOutput    ( "itemCount", actualResult );

        if ( actualResult != expectedResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/


    FinishTestSet();
    return TestResult();
}

int StorageTester::Test_UpdateItem()
{
    StartTestSet( "StorageTester::UpdateItem", { } );
    Logger::Out( "Beginning test", "StorageTester::UpdateItem" );

    { /* TEST BEGIN ************************************************************/
        StartTest( "Update item and verify the update" );

        const int AS = 5;
        int ic = 4;
        string arr[AS] = { "A", "B", "C", "D" };

        UpdateItem( 2, "Z", arr, ic, AS );

        string expectedResult = "Z";
        string actualResult = arr[2];

        Set_ExpectedOutput  ( "arr[2]", expectedResult );
        Set_ActualOutput    ( "arr[2]", actualResult );

        if ( actualResult != expectedResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int StorageTester::Test_ClearAllItems()
{
    StartTestSet( "StorageTester::ClearAllItems", { } );
    Logger::Out( "Beginning test", "StorageTester::ClearAllItems" );

    { /* TEST BEGIN ************************************************************/
        StartTest( "Add items to array and clear" );

        const int AS = 5;
        int ic = 5;
        string arr[AS] = { "a", "b", "c", "d", "e" };

        ClearAllItems( arr, ic, AS );

        bool allClear = true;
        for ( int i = 0; i < AS; i++ )
        {
            Set_ExpectedOutput  ( "arr[" + StringUtil::ToString(i) + "]", "" );
            Set_ActualOutput    ( "arr[" + StringUtil::ToString(i) + "]", arr[i] );

            if ( arr[i] != "" )
            {
                allClear = false;
            }
        }


        if ( !allClear )   { TestFail(); }
        else               { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "Clear resets itemCount to 0" );

        const int AS = 5;
        int ic = 5;
        string arr[AS] = { "a", "b", "c", "d", "e" };

        ClearAllItems( arr, ic, AS );

        int expectedResult = 0;
        int actualResult = ic;

        Set_ExpectedOutput  ( "itemCount", expectedResult );
        Set_ActualOutput    ( "itemCount", actualResult );

        if ( actualResult != expectedResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int StorageTester::Test_SaveItems()
{
    StartTestSet( "StorageTester::SaveItems", { } );
    Logger::Out( "Beginning test", "StorageTester::SaveItems" );

    bool fileCreated = true;
    { /* TEST BEGIN ************************************************************/
        StartTest( "Try to save file, verify file was created." );

        const int AS = 5;
        int ic = 0;
        string arr[AS] = { "a" };

        SaveItems( "test.txt", arr, ic );

        ifstream input( "test.txt" );
        Set_ExpectedOutput  ( "Text file created", "created" );
        if ( input.fail() )
        {
            Set_ActualOutput    ( "Text file created", "not created" );
            fileCreated = false;
        }
        else
        {
            Set_ActualOutput    ( "Text file created", "created" );
        }

        if ( !fileCreated ) { TestFail(); }
        else                { TestPass(); }
        input.close();

        FinishTest();
    } /* TEST END **************************************************************/

    if ( !fileCreated )
    {
        FinishTestSet();
        return TestResult();
    }

    { /* TEST BEGIN ************************************************************/
        StartTest( "Create array, save, and verify" );

        const int AS = 5;
        int ic = 5;
        string arr[AS] = { "a", "b", "c", "d", "e" };

        SaveItems( "test.txt", arr, ic );

        bool allMatch = true;

        ifstream input( "test.txt" );
        if ( input.fail() )
        {
            allMatch = false;
        }

        string buffer = "";
        for ( ic = 0; ic < 5; ic++ )
        {
            getline( input, buffer );
            Set_ExpectedOutput  ( "Line " + StringUtil::ToString(ic) + " of file ", arr[ic] );
            Set_ActualOutput    ( "Line " + StringUtil::ToString(ic) + " of file ", buffer );

            if ( buffer != arr[ic] )
            {
                allMatch = false;
            }
        }

        if ( !allMatch )   { TestFail(); }
        else               { TestPass(); }
        input.close();

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int StorageTester::Test_LoadItems()
{
    StartTestSet( "StorageTester::LoadItems", { } );
    Logger::Out( "Beginning test", "StorageTester::LoadItems" );

    { /* TEST BEGIN ************************************************************/
        StartTest( "Create and save file, load from file and verify" );

        const int AS = 5;
        int ic = 0;
        string arr[AS] = {};
        string expected[AS] = { "item one", "item two", "item three" };

        ofstream output( "test.txt" );
        output  << "item one" << endl
                << "item two" << endl
                << "item three" << endl;
        output.close();

        LoadItems( "test.txt", arr, ic );

        bool allMatch = true;
        for ( int i = 0; i < AS; i++ )
        {
            Set_ExpectedOutput  ( "arr[" + StringUtil::ToString(i) + "]", expected[i] );
            Set_ActualOutput    ( "arr[" + StringUtil::ToString(i) + "]", arr[i] );

            if ( arr[i] != expected[i] )
            {
                allMatch = false;
            }
        }

        int expectedCount = 3;
        int actualCount = ic;

        Set_ExpectedOutput  ( "itemCount", expectedCount );
        Set_ActualOutput    ( "itemCount", actualCount );

        if ( !allMatch )                            { TestFail(); }
        else if ( expectedCount != actualCount )    { TestFail(); }
        else                                        { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}


#endif


